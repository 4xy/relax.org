# Dockerfile
# Relax slides www

FROM python:3-onbuild

MAINTAINER Relax Foundation

RUN apt-get update

ADD app /opt/relax

EXPOSE 8080

WORKDIR /opt/relax

CMD ["python3", "/opt/relax/app.py", "--address=*"]
