//////////////////////////////////////////////////////////////////////////
// texture_map.js
//
// Fenix Games general purpose library: HTML5 LimeJS Based Graphics.
//
// Load the image as two dimensional array of images.
//
// 2013, (c) Fenix Games Foundation.

/** @file Represents an image as two-dimensional array, provides
 *    an access to the certain cell.
 */

goog.provide('fg.res.TextureMap');
goog.provide('fg.res.events');

goog.require('fg.debug.Logger');

goog.require('lime.fill.Image');
goog.require('lime.fill.Frame');

goog.require('goog.math');
goog.require('goog.events');

/** Textures map.
 *
 *  Represents an image as two-dimensional array, provides an access to the
 *  certain cell.
 *
 *  @constructor
 *  @param { String | lime.fill.Image } asset The either path to the image to
 *    load or the already loaded image.
 *
 *  @param { Number | goog.math.Size } cellSize The size of the image element.
 *    If zero or unspecified it's assumed the map has only one cell i.e. entire
 *    image represents one cell with coordinates (0, 0). Optional
 *
 *  @param { Boolean } autoLoad Designates whether to initiate image loading
 *    automatically in the constructor. This only effects if the asset parameter
 *    is the path to the image. Optional, default = true.
 */
fg.res.TextureMap = function(asset, cellSize, autoLoad)
{
  goog.base(this);
  goog.object.extend(this, new fg.debug.Logger("fg.res.TextureMap"));

  if (!goog.isDefAndNotNull(cellSize)) { cellSize = 0; }
  if (!goog.isDefAndNotNull(autoLoad)) { autoLoad = true; }

  if (goog.isNumber(cellSize))
  {
    cellSize = new goog.math.Size(cellSize, cellSize);
  }

  this._cell_size = cellSize;
  this._asset     = asset;
  this._cache     = {};
  this._inited    = false;

  if (goog.isDefAndNotNull(asset) && autoLoad) { this.load(asset); }
}

goog.inherits(fg.graph.TextureMap, goog.events.EventTarget);

fg.res.events = { INITIALIZED: "initialized" };

/** Loads an image.
 *
 *  @param { String | lime.fill.Image } asset The either path to the image to
 *    load or the already loaded image. Optional, default = value passed to constructor.
 */
fg.res.TextureMap.prototype.load = function(asset)
{
  this.writeLogInfo("Start loading assets...");

  asset = asset || this._asset;

  if (!goog.isDefAndNotNull(asset))
  {
    throw new Error("The given asset is invalid");
  }

  if (asset instanceof lime.fill.Image)
  {
    this._initialize(asset);
  } else
  {
    var image = new lime.fill.Image(asset);

    goog.events.listen(image, goog.events.EventType.LOAD,
      function onLoad(e)
      {
        goog.events.unlisten(image, goog.events.EventType.LOAD, onLoad);
        this._initialize(image);
      }, false, this);
  }
}

/** Whether texture initialized and valid. */
fg.res.TextureMap.prototype.isValid = function()
{
  return this._asset instanceof lime.fill.Image;
}

/** Returns the cell with the given index.
 *
 *  @param { Number } col Horizontal coordinate of the cell. Optional, default = 0.
 *  @param { Number } row Vertical coordinate of the cell. Optional, default = 0.
 */
fg.res.TextureMap.prototype.getCell = function(col, row)
{
  if (!this.isValid()) { throw new Error("The asset is invalid"); }

  if (!goog.isDefAndNotNull(col)) { col = 0; }
  if (!goog.isDefAndNotNull(row)) { row = 0; }

  if (!this.cellExists(col, row)) { return null; }

  const pos = new goog.math.Coordinate(col, row);
  const img = this._asset.getImageElement();
  const csz = this._cell_size.isEmpty()?
    new goog.math.Size(img.width, img.height): this._cell_size;

  var cache = this._cache;
  if (!cache[pos])
  {
    cache[pos] = new lime.fill.Frame(img, pos.x * csz.width, pos.y * csz.height,
      csz.width, csz.height);
  }

  return cache[pos];
}

/** Whether the cell with given coordinates exists.
 *
 *  @param { Number } col Horizontal coordinate of the cell.
 *  @param { Number } row Vertical coordinate of the cell.
 */
fg.res.TextureMap.prototype.cellExists = function(col, row)
{
  const pos = new goog.math.Coordinate(col, row);
  const csz = this._cell_size;
  const img = this._asset.getImageElement();

  const map  = new goog.math.Rect(0, 0, img.width, img.height);
  const cell = new goog.math.Rect(pos.x * csz.width, pos.y * csz.height,
    csz.width, csz.height);

  return map.contains(cell);
}

/** Whether the map initialized. */
fg.res.TextureMap.prototype.isInitialized = function() { return this._inited; }

/** Initializes the map.
 *
 *  @private
 *  @param { lime.fill.Image } image An image to use as textures map.
 */
fg.res.TextureMap.prototype._initialize = function(image)
{
  this._asset  = image;
  this._cache  = {};
  this._inited = true;

  this._writeLogInfo("Initialized");

  this.dispatchEvent({type: graph.events.INITIALIZED});
}

//------------------------------------------------------------------------
//fg.res.TextureMap.prototype._writeLogInfo = function()
//{
//  this._logger.writeLogInfo.apply(this._logger, arguments);
//}


