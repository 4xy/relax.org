//////////////////////////////////////////////////////////////////////////
// std_extend.js
//
// Fenix Games general purpose library.
//
// JavaScript standard extensions.
//
// 2013, (c) Fenix Games Foundation.

/** @file std_extend.js JavaScript standard extensions. */

// provide dummy object
//goog.provide('fg.extensions');
window.goog && window.goog.provide('fg.extensions');

(function()
{
  //first, checks if it isn't implemented yet
  if (!String.prototype.format)
  {
    String.prototype.format = function()
    {
      var args = arguments;

      return this.replace(/{(\d+)}/g,
        function (match, number)
        {
          return typeof args[number] != 'undefined'? args[number]: match;
        });
    };
  }
})();