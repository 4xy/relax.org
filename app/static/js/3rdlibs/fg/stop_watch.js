//////////////////////////////////////////////////////////////////////////
// stop_watch.js
//
// Fenix Games general purpose library.
//
// Stop watch. Measures time interval.
//
// 2013, (c) Fenix Games Foundation.

/** @file Stop watch. */

goog.provide('fg.utils.StopWatch');

/** Creates stop watch instance.
 *
 *  @param { Boolean } performance If !false (strictly) tries
 *    to use window.performance.
 */
fg.utils.StopWatch = function (performance)
{
  this._startTime = 0;
  this._stopTime = 0;
  this._running = false;
  this._perf = performance === false? false : !!window.performance;
};

/** Returns the same as window.performance.now() or Date.getTime() in case
 *  if performance parameter on constructor is false.
 */
fg.utils.StopWatch.prototype.currentTime = function ()
{
  return this._perf ? window.performance.now(): new Date().getTime();
};

/** Whether stop watch is running. */
fg.utils.StopWatch.prototype.isRunning = function()
{
  return this._running;
}

/** Starts measuring. */
fg.utils.StopWatch.prototype.start = function ()
{
  this._startTime = this.currentTime();
  this._running = true;
};

/** Stops measuring. */
fg.utils.StopWatch.prototype.stop = function ()
{
  this._stopTime = this.currentTime();
  this._running = false;
};

/** Returns elapsed time from start or previous call of getElapsedMilliseconds
 *  measured in milliseconds
 *
 *  @param { Boolean } resetStartTime if true updates the start time to
 *    current moment.
 */
fg.utils.StopWatch.prototype.getElapsedMilliseconds = function (resetStartTime)
{
  if (this._running)
  {
    this._stopTime = this.currentTime();
  }

  if (resetStartTime)
  {
    var tmp         = this._startTime;
    this._startTime = this._stopTime;
    return this._stopTime - tmp;
  } else
  {
    return this._stopTime - this._startTime;
  }
};

/** Returns the same as getElapsedMilliseconds divided by 1000.
 *
 *  @param { Boolean } resetStartTime if true updates the start time to
 *    current moment.
 */
fg.utils.StopWatch.prototype.getElapsedSeconds = function (resetStartTime)
{
  return this.getElapsedMilliseconds(resetStartTime) / 1000;
};

//StopWatch.prototype.printElapsed = function (name)
//{
//  var currentName = name || 'Elapsed:';
//
//  console.log(currentName, '[' + this.getElapsedMilliseconds() + 'ms]', '[' + this.getElapsedSeconds() + 's]');
//};