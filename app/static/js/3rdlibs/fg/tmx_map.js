//////////////////////////////////////////////////////////////////////////
// map.js
//
// Fenix Games general purpose library.
//
// Game map.
//
// 2013, (c) Fenix Games Foundation.

/** @file Game map. */

goog.provide('fg.game.Map');

//goog.require('fg.packman.d.makeAssetsPath');

//goog.require('fg.res.TextureMap');

//goog.require('lime.Sprite');
goog.require('lime.Layer');
goog.require('lime.parser.TMX');

//
//goog.require('goog.events');
//goog.require('goog.asserts');
//goog.require('goog.array');
goog.require('goog.object');
//goog.require('goog.math.Size');
//goog.require('lime.scheduleManager');

/** Game map.
 *
 *  Two dimensional map of the level. If no parameters specified the load will
 *  not attempted.
 *
 *  @param { String } fileName TMX File name contains map information and resources.
 */
fg.game.TMXMap = function(fileName)
{
  goog.base(this);
  goog.object.extend(this, new fg.debug.Logger("fg.game.Map"));

  if (goog.isDefAndNotNull()) { this.load(fileName); }
}

goog.inherits(fg.game.Map, lime.Layer);
//goog.inherits(oc.slot.Slot, lime.Layer);

/** Loads the map.
 *
 *  @param { String } fileName TMX File name contains map information and resources.
 */
fg.game.Map.prototype.load = function(fileName)
{
  this._initialize(fileName);
}

//////////////////////////////////////////////////////////////////////////
// private stuff

fg.game.Map.prototype._initialize = function(fileName)
{
  this.writeLogInfo("Initializing... {0}", fileName);

  var tmx = new lime.parser.TMX(fileName);
//  for(var j = 0; j < tmx.layers.length; j++)
//  {
//    for(var i = 0; i < tmx.layers[j].tiles.length; i++)
//    {
//      tile = tmx.layers[j].tiles[i];
//      sprite = new lime.Sprite().setPosition(tile.px,tile.py);
//      sprite.setFill(tile.tile.frame);
//      layer.appendChild(sprite);
//    }
//  }

//    goog.asserts.assert(params.stripsNumber() == params.freeSpinStrips().length,
//        "Invalid configuration. Free spin strips number are not identical to declared.");
}

