//////////////////////////////////////////////////////////////////////////
// button.js
//
// Fenix Games general purpose library: HTML5 LimeJS Based Graphics.
//
// Simple image button based on lime.Button.
//
// 2013, (c) Fenix Games Foundation.

/** @file button.js Simple image button. */

goog.provide('fg.ui.Button');

goog.require('lime.fill.Frame');
goog.require('lime.fill.Image');
goog.require('lime.Button');
goog.require('lime.Sprite');

goog.require('goog.math.Rect');

/** Button constructor.
 *
 *  @constructor
 *  @param {string | lime.fill.Image} skin Path to skin image or already loaded image itself.
 *    Skin is the set of the glyphs vertically arranged. Each glyph represents
 *    the state of the button: IDLE (I), DOWN (D), HOVER (H), DISABLED (DD).
 *
 *  @param { number } glyphs_num Designates how many states are in the image available.
 *    Next scheme shows how the states relates to the presented glyphs.
 *    4 - I, D, H, DD; 3 - I, D, H; 2 - I&H, D; 1 - I&H&D. Default is 3.
 */
fg.ui.Button = function(skin, glyphs_num)
{
  if (glyphs_num == undefined) { glyphs_num = 3; }

  if ([1, 2, 3, 4].indexOf(glyphs_num) == -1)
  {
    throw new Error("Argument error: The number of glyphs should be in [1, 4]");
  }

  this._glyphs_num  = glyphs_num;
  this._up_sprite   = new lime.Sprite().setAnchorPoint(0, 0);
  this._down_sprite = new lime.Sprite().setAnchorPoint(0, 0);
  this._disabled_sprite = new lime.Sprite().setAnchorPoint(0, 0);

  this._up_sprite._parent   = this;
  this._down_sprite._parent = this;

  goog.base(this, this._up_sprite, this._down_sprite);

  this._initialize(skin);
}

goog.inherits(fg.ui.Button, lime.Button);

/** Disabled button state. */
//lime.Button.State.DISABLED = 2;

/** Enables or disables the button.
 *
 *  @param { Boolean } state Button state.
 */
fg.ui.Button.prototype.enable = function(state)
{
  this._enabled = state;

  var s = this._disabled_sprite;
  if (!state) { this.appendChild(s) } else { this.removeChild(s); }
}

/** Whether button enabled. */
fg.ui.Button.prototype.enabled = function()
{
  return this._enabled;
}

/** Workaround on LimeJS over & out events. */
fg.ui.Button.prototype.attach_listeners = function()
{
  var scene = this.getScene();

  if (scene)
  {
    scene.listenOverOut(this._up_sprite, this._up_mouse_over_handler, this._up_mouse_out_handler);
    scene.listenOverOut(this._down_sprite, this._down_mouse_over_handler, this._down_mouse_out_handler);
  }

  return this;
}

//////////////////////////////////////////////////////////////////////////
// private stuff

//------------------------------------------------------------------------
// mouse handlers STARTED
fg.ui.Button.prototype._up_mouse_over_handler = function(e)
{
  oc.ui.console_log("[oc.ui.Button]: Mouse OVER: (%d, %d)",
      e.position.x.toFixed(), e.position.y.toFixed());

  this.setFill(this._parent._hover_image);
}

//------------------------------------------------------------------------
fg.ui.Button.prototype._up_mouse_out_handler = function(e)
{
  oc.ui.console_log("[oc.ui.Button]: Mouse OUT: (%d, %d)",
      e.position.x.toFixed(), e.position.y.toFixed());

  this.setFill(this._parent._idle_image);
}

//------------------------------------------------------------------------
fg.ui.Button.prototype._down_mouse_over_handler = function(e)
{
  oc.ui.console_log("[oc.ui.Button]: Mouse OVER: (%d, %d)",
      e.position.x.toFixed(), e.position.y.toFixed());

  this.setFill(this._parent._down_image);
}

//------------------------------------------------------------------------
fg.ui.Button.prototype._down_mouse_out_handler = function(e)
{
  oc.ui.console_log("[oc.ui.Button]: Mouse OUT: (%d, %d)",
      e.position.x.toFixed(), e.position.y.toFixed());

  this.setFill(this._parent._idle_image);
}
// mouse handlers FINISHED
//------------------------------------------------------------------------
fg.ui.Button.prototype._initialize = function(skin)
{
  this._enabled = true;

  this._load_skin(skin);

  // fixme: How to cancel event on current object
//  goog.events.listen(this, lime.Button.Event.CLICK,
//    function(e)
//    {
//      oc.ui.console_log("[oc.ui.Table]: Button click");
//
//      if (!this.enabled())
//      {
//        e.stopPropagation();
//        e.preventDefault();
//        e.returnValue = false;
//        if (e.event)
//        {
//          e.event.stopPropagation();
//          e.event.preventDefault();
//          e.event.returnValue = false;
//          e.event.cancelBubble = true;
//        }
//      }
//    });
}

////------------------------------------------------------------------------
//oc.ui.Button.prototype._setup_state_sprites = function()
//{
//
//}

//------------------------------------------------------------------------
fg.ui.Button.prototype._load_state_images = function(image_map)
{
  const img = image_map.getImageElement();
  const h   = Math.floor(img.height / this._glyphs_num);
  const w   = img.width;
  const tmp = ["_idle_image", "_down_image", "_hover_image", "_disabled_image"];

  for (var i = 0; i < this._glyphs_num; ++i)
  {
//    this[tmp[i]] = new lime.fill.Frame(img, new goog.math.Rect(0, i * h, w, h),
//      new goog.math.Vec2(0, 0), new goog.math.Size(w, h));
    this[tmp[i]] = new lime.fill.Frame(img, new goog.math.Rect(0, i * h, w, h),
        new goog.math.Vec2(0, 0), new goog.math.Size(w, h));
  }

  fg.debug.console_assert(this._idle_image != undefined,
    "[oc.ui.Button]: Invalid idle image");

  for (var i = 0; i < 4; ++i)
  {
    if (this[tmp[i]] == undefined) { this[tmp[i]] = this._idle_image; }
  }

  var sz = this.getSize();
  if (sz.isEmpty()) { this.setSize(w, h); sz = this.getSize(); }

  this._up_sprite.setFill(this._idle_image).setSize(sz.clone());
  this._down_sprite.setFill(this._idle_image).setSize(sz.clone());
  this._disabled_sprite.setFill(this._disabled_image).setSize(sz.clone());
}

//------------------------------------------------------------------------
fg.ui.Button.prototype._load_skin = function(skin)
{
  if (skin instanceof lime.fill.Image)
  {
    this._load_state_images(skin);
  } else
  {
    var image = new lime.fill.Image(skin);

    goog.events.listen(image, goog.events.EventType.LOAD,
      function(e)
      {
        //oc.ui.console_log("[oc.ui.FieldItem]: Asset loaded: %s", image_fn);
        this._load_state_images(image);
      }, false, this);
  }
}
