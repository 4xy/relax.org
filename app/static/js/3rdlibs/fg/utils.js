//////////////////////////////////////////////////////////////////////////
// utils.js
//
// Fenix Games general purpose library.
//
// Common utilities.
//
// 2013, (c) Fenix Games Foundation.

goog.provide('fg.utils');

/** Creates an array that contains unique elements for both arrays.
 *
 *  Elements must have equals() function.
 */
fg.utils.not_intersect = function(a, b)
{
  if (a == undefined) { a = []; }
  if (b == undefined) { b = []; }

  return goog.array.filter(a,
    function(el, i, v)
    {
      var res = el != null;
      for (var j = 0; j < b.length && res; ++j) { res = !el.equals(b[j]); }
      //for (var j = 0; j < b.length && res; ++ j) { res = el != b[j]; }
      return res;
    }
  ).concat(b);
}
