//////////////////////////////////////////////////////////////////////////
// tile.js
//
// Fenix Games general purpose library.
//
// Game map tile.
//
// 2013, (c) Fenix Games Foundation.

/** @file Game map tile. */

goog.provide('fg.game.Tile');

goog.require('lime.Sprite');
//goog.require('lime.Layer');

goog.require('goog.object');
//goog.require('goog.events');
//goog.require('goog.asserts');
//goog.require('goog.array');
//goog.require('goog.math.Size');
//goog.require('lime.scheduleManager');

/** Game map tile.
 *
 *  Represents the tile of the map.
 */
fg.game.Tile = function()
{
  goog.base(this);
  goog.object.extend(this, new fg.debug.Logger("fg.game.Map"));
}

goog.inherits(fg.game.Tile, lime.Sprite);

//////////////////////////////////////////////////////////////////////////
// private stuff

fg.game.Map.prototype._initialize = function(params)
{
  this.writeLogInfo("Initializing...");

//    goog.asserts.assert(params.stripsNumber() == params.freeSpinStrips().length,
//        "Invalid configuration. Free spin strips number are not identical to declared.");
}
