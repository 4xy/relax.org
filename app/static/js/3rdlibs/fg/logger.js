//////////////////////////////////////////////////////////////////////////
// logger.js
//
// Fenix Games general purpose library.
//
// Debug logger.
//
// 2013, (c) Fenix Games Foundation.

/** @file Debug logger. */

goog.provide('fg.debug.Logger');

goog.require('goog.array');
goog.require('goog.debug.Logger');

/** @class Logger Wraps goog.debug.Logger. */

/** Creates an instance of the oc.debug.Logger. */
fg.debug.Logger = function(name)
{
  this._name = name;
}

/** Returns logger instance name. */
fg.debug.Logger.prototype.name = function()
{
  return this._name;
}

/** Writes an information to the debug console. */
fg.debug.Logger.prototype.writeLogInfo = function(s)
{
  if (goog.DEBUG)
  {
    if (arguments.length > 1)
    {
      s = s.format.apply(s, goog.array.toArray(arguments).slice(1));
    }

    goog.debug.Logger.getLogger(this._name).info(s);
  }
}


