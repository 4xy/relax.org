function ChatWebSocket(onOpen, onMessage, onClose, onError)
{
  var ws = new WebSocket("ws://{0}/chat/".format(window.location.host));
  ws.onopen = onOpen;
  ws.onmessage = onMessage;
  ws.onclose = onClose;
  ws.onerror = onError;

  this._ws = ws;
}

ChatWebSocket.prototype.send = function(message)
{
  this._ws.send(JSON.stringify(message));
}