//////////////////////////////////////////////////////////////////////////
// std-extend.js
//
// Fenix Games general purpose library.
//
// JavaScript standard extensions.
//
// 2013, (c) Fenix Games Foundation.

/** @file std-extend.js JavaScript standard extensions. */

// provide dummy object
window.goog && window.goog.provide('fg.extensions');

(function()
{
  //first, checks if it isn't implemented yet
  if (!String.prototype.format)
  {
    String.prototype.format = function()
    {
      var args = arguments;

      return this.replace(/{(\d+)}/g,
        function (match, number)
        {
          return typeof args[number] != 'undefined'? args[number]: match;
        });
    };
  }
})();

function parseBool(string) {
    if (string == null) { string = ""; }

    switch(string.toLowerCase().trim()){
        case "true": case "yes": case "1": return true;
        case "false": case "no": case "0": case null: return false;
        default: return Boolean(string);
    }
}