import json
import tornado
import tornado.web
import collections

from flask import render_template, make_response, request, session, Response, stream_with_context, abort, jsonify
from tornado.websocket import WebSocketHandler

from flask_app import app as flask_app

import settings


class Storage:
    """Chat data storage.

    Used to show history tail to new connected clients only.
    """

    # keep 10 recent message only
    SIZE = 10

    # -------------------------------------------------------------------------
    def __init__(self):
        # self.__queue = collections.deque
        self._queue = []

    # -------------------------------------------------------------------------
    def tail(self):
        return self._queue[-Storage.SIZE:]

    # -------------------------------------------------------------------------
    def push(self, message):
        queue = self._queue
        queue.append(message)
        if len(queue) > Storage.SIZE:
            self._queue = self.tail()

class WebSocket(WebSocketHandler):
    """Serves web site chat."""

    storage = Storage()

    def __init__(self, app, *args, **kwargs):
        super().__init__(app, *args, **kwargs)
        self._app = app
        self._storage = WebSocket.storage

    # -------------------------------------------------------------------------
    def open(self):
        print("New web socket connection")

        app = self._app
        app.chat_clients.add(self)
        tail = self._storage.tail()
        for message in tail:
            self.write_message(message)

        # tornado.ioloop.IOLoop.instance().add_callback(self.process)

    # -------------------------------------------------------------------------
    def on_message(self, message):
        print("Received message: " + message)

        self._storage.push(message)
        clients = self._app.chat_clients
        for client in clients:
            client.write_message(message)

    # -------------------------------------------------------------------------
    def on_close(self):
        print("Web socket closed")
        self._app.chat_clients.remove(self)






