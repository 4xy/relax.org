from flask import Flask
from flask_babel import Babel
from flask_session import Session
from babel.support import Translations
from werkzeug.contrib.fixers import ProxyFix

import settings

app = Flask(__name__)
# https fix, make flask use X-Forwarded-Proto header issued by nginx
app.wsgi_app = ProxyFix(app.wsgi_app)
app.secret_key = '\x81\xb6\xee\xbdep\xff\\\xfbu\xec~R\xb8S\x12\xddm0\x0e\xdc\xdc\x07\xee'
babel = Babel(app)
Translations.DEFAULT_DOMAIN = "strings"
app.config['BABEL_DEFAULT_LOCALE'] = settings.DEFAULT_LOCALE
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
