from channel import ChannelCollection
from channel import ChannelOperationType
from channel import ChannelType
from channel import ChannelContent

from flask import session, render_template, abort, request, make_response

import tornado
import tornado.escape
import os
import os.path

from flask_app import app
import settings
import channel
import sendmail

import json

# ---------------------------------------------------------------------
def is_content_cached(content_type, path):
    return content_type in session and path in session[content_type]

# ---------------------------------------------------------------------
def get_cached_content(content_type, path):
    if is_content_cached(content_type, path):
        return session[content_type][path]

    return None

# ---------------------------------------------------------------------
def set_cached_content(content_type, path, content):
    if content_type not in session:
        session[content_type] = {}

    session[content_type][path] = content

# ---------------------------------------------------------------------
def get_channel_content(channels, channel_name, path):
    if not is_content_cached(channel.CHANNEL_CONTENT, path):
        ch = channels.get_channel(channel_name)
        raw_content = [tornado.escape.url_escape(fn) for fn in ch.content() if fn.endswith(".mp3")]
        content = ChannelContent(raw_content, ch.directory(False))
        set_cached_content(channel.CHANNEL_CONTENT, path, content)
        return content
    else:
        return get_cached_content(channel.CHANNEL_CONTENT, path)

# ---------------------------------------------------------------------
def audio_fn_to_source(fn):
        return tornado.escape.json_encode({
            "title": os.path.splitext(os.path.basename(fn))[0],
            "mp3": fn,
            "oga": os.path.splitext(fn)[0] + ".ogg"
        }) if fn else "{}"

# ---------------------------------------------------------------------
def obtain_channels_col():
    if "channels" not in session:
        col = ChannelCollection(settings.CHANNELS_PATH)
        session['channels'] = col
        return col

    col = session['channels']
    channels = col.channels()
    if not channels:
        col = ChannelCollection(settings.CHANNELS_PATH)
        session['channels'] = col
        channels = col.channels()
        if not channels:
            abort(500)

    assert col, "Invalid collection"
    return col


@app.route('/')
def root():
    """Site's root handler."""

    def make_menu_items(ch_list):
        result = {item: {"name": item} for item in ch_list}
        return json.dumps(result, sort_keys=True)

    col = obtain_channels_col()

    vchannel_set = col.channels(ChannelType.VISUAL)
    achannel_set = col.channels(ChannelType.AUDIO)

    cur_vch = vchannel_set.current_channel()
    cur_ach = achannel_set.current_channel()

    visual_channels = make_menu_items(vchannel_set.channels_str_list())
    audio_channles = make_menu_items(achannel_set.channels_str_list())

    def get_ach_content():
        ch_name = cur_ach.channel_name()
        return get_channel_content(achannel_set, ch_name,
                                   "/{0}/{1}/{2}/".format(settings.CHANNELS_DIR_NAME,
                                                          ChannelType.AUDIO, ch_name))

    audio_fn = get_ach_content().current()
    audio_source = audio_fn_to_source(audio_fn)

    return render_template('index.html',
                           print_visual_sources=lambda: cur_vch.sources(cur_vch.current_item()),
                           audio_source=audio_source,
                           visual_channel_items=visual_channels,
                           audio_channel_items=audio_channles,
                           cur_visual_channel=cur_vch.channel_name(),
                           cur_audio_channel=cur_ach.channel_name())


# def _set_ch_cur_item(resp, ch_name, ch_type, ch_item):
#     resp.set_cookie("{}-{}-channel-item".format(ch_name, ch_type), ch_item)

# ---------------------- -----------------------------------------------
def _handle_visual_channels(channels, channel_name, asset):
    if not ChannelOperationType.is_list(asset):
        abort(404)

    ch = channels.get_channel(channel_name)
    item = ch.current_item()
    return ch.sources(item)

# ---------------------------------------------------------------------
def _handle_audio_channels(channels, channel_name, asset):
    def get_ch_content():
        return get_channel_content(channels, channel_name, os.path.split(os.path.split(request.path)[0])[0] + "/")

    if ChannelOperationType.is_list(asset):
        ch = channels.get_channel(channel_name)
        item = ch.current_item()
        content = ch.content(item)
        return content
    elif ChannelOperationType.is_current(asset):
        return audio_fn_to_source(get_ch_content().current())
    elif ChannelOperationType.is_next(asset):
        return audio_fn_to_source(get_ch_content().next().current())
    elif ChannelOperationType.is_prev(asset):
        return audio_fn_to_source(get_ch_content().prev().current())
    else:
        raise abort(404)


@app.route('/channels/<channel_type>/<channel_name>/<asset>/')
def slider_handler(channel_type, channel_name, asset):
    col = obtain_channels_col()
    assert "channels" in session, "Channels collection not found in session object"

    channels = col.channels(channel_type)

    if ChannelType.is_visual_channel(channel_type):
        s = _handle_visual_channels(channels, channel_name, asset)
    elif ChannelType.is_audio_channel(channel_type):
        s = _handle_audio_channels(channels, channel_name, asset)
    else:
        abort(404)

    return s

@app.route('/feedback/', methods=['POST'])
def feedback():
    try:
        name = request.form["name"]
        email = request.form["email_address"]
        message = request.form["message"]

        sendmail.send("root@relax4u.org", settings.CONTACT_EMAIL,
                      "Feedback from user; '{name}'; e-mail: '{email}'".format(name=name, email=email), message)
    except Exception as e:
        print("ERROR: Send mail error: {}".format(str(e)))
        return abort(500)

    return make_response('', 204)
