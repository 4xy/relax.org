from os.path import join, islink, realpath
import os
import random
import string
import binascii
import sys

import settings
import subprocess

from channel import ChannelCollection, ChannelType

# 800 kb
IMAGE_SIZE_LIMIT = 800

# 857 kb - 55%
# 1500 kb - 65%
# (65 - 55) / (1500 - 857) = 0,0155521
# bx + a = y
# 0,0155521 * 1000 + 41,6718503 = 57,2239503%


def ratio(size):
    x1, y1 = 857, 80
    x2, y2 = 1500, 70
    b = (y2 - y1) / (x2 - x1)
    a = y1 - b * x1
    return int(size * b + a)


hash = "{:x}".format(binascii.crc32(b"relax4u.org"))

total_files = 0
total_audio_files = 0
total_visual_files = 0
audio_files_processed = 0
visual_files_processed = 0
total_links = 0
total_audio_links = 0
total_visual_links = 0

def process_audio(col):
    global total_files, total_audio_files, audio_files_processed, total_links, total_audio_links

    channel_set = col.channels(ChannelType.AUDIO)
    channel_list = channel_set.channels_list()

    def process_file(fn):
        global total_files, total_audio_files, audio_files_processed, total_links, total_audio_links
        nonlocal ch

        src_full_fn = join(ch.directory(), fn)
        if islink(src_full_fn):
            total_links += 1
            total_audio_links += 1
        else:
            total_files += 1
            total_audio_files += 1

        if fn.startswith(hash):
            return

        fn_parts = fn.replace('_', '-').split('-')
        fn = '-'.join([part.strip() for part in fn_parts])
        fn_parts = fn.split(' ')
        fn = '-'.join([part.strip() for part in fn_parts if part.strip() != ''])
        dest_full_fn = join(ch.directory(), "{}-{}".format(hash, fn.lower()))
        print(src_full_fn + ' -> ' + dest_full_fn)
        os.rename(src_full_fn, dest_full_fn)
        audio_files_processed += 1

    # audio
    for ch_name in sorted(channel_list.keys()):
        ch = channel_list[ch_name]
        for fn in ch.content():
            process_file(fn)


def process_visual(col):
    global total_files, total_visual_files, visual_files_processed, total_links, total_visual_links

    channel_set = col.channels(ChannelType.VISUAL)
    channel_list = channel_set.channels_list()

    def process_file(fn):
        global total_files, total_visual_files, visual_files_processed, total_links, total_visual_links
        nonlocal ch

        src_full_fn = join(ch.directory(), fn)
        if islink(src_full_fn):
            total_links += 1
            total_visual_links += 1
        else:
            total_files += 1
            total_visual_files += 1

        if fn.startswith(hash):
            return

        tmp, ext = os.path.splitext(fn)
        dest_full_fn = join(ch.directory(), '{}-{}{}'.format(hash,
                            ''.join([random.choice(string.ascii_lowercase) for i in range(16)]), ext))

        size_ratio = 0
        file_sz = os.path.getsize(src_full_fn) / 1024
        if file_sz > IMAGE_SIZE_LIMIT:
            size_ratio = ratio(file_sz)
            if size_ratio < 0:
                size_ratio = 70
            subprocess.run(['convert', src_full_fn, '-strip', '-resize', '{}%'.format(size_ratio), src_full_fn])

        print('{} -> {}; size: {:.2f} kb; {:.2f}%'.format(src_full_fn, dest_full_fn, file_sz, size_ratio))
        os.rename(src_full_fn, dest_full_fn)
        visual_files_processed += 1

    # pictures
    for ch_name in channel_list.keys():
        ch = channel_list[ch_name]

        for fn in ch.content():
            process_file(fn)

# main entry point
def main():
    col = ChannelCollection(settings.CHANNELS_PATH)
    process_audio(col)
    process_visual(col)


def print_report(error=False):
    print("Done with errors" if error else "Done")
    print("------------------------------+------+------+------")
    print("Total files, audio, visual    | {:5}| {:5}| {:5}".format(total_files, total_audio_files, total_visual_files))
    print("Total processed, audio, visual| {:5}| {:5}| {:5}".format(audio_files_processed + visual_files_processed, audio_files_processed, visual_files_processed))
    print("Total links, audio, visual    | {:5}| {:5}| {:5}".format(total_links, total_audio_links, total_visual_links))

if __name__ == "__main__":
    try:
        main()
        print_report()
    except Exception as e:
        print("An error occurred: {0}".format(str(e)))
        print_report(True)
        sys.exit(1)
    except:
        print("An unknown error occurred!")
        print_report(True)
        sys.exit(1)


