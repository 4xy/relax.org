"""channel.py

Relax slides project.

Media channel.

2014 (c), Relax Foundation.
"""


import tornado.escape
import tornado
import os
import os.path
import logging

from collections import deque
from os.path import join
from os.path import isdir, isfile
# from random import shuffle
from flask import request
import settings

# strings file list
RAW_CONTENT = "raw-content"
# file list wrapped within html tags necessary by the media player
SOURCES_CONTENT = "sources-content"
# channel.ChannelContent class
CHANNEL_CONTENT = "channel-content"
DEFAULT_ROOT = 'channels'


class ChannelOperationType:
    """Public operation type available by url."""

    LIST = "list"
    LIST_SOURCES = "list-sources"
    CURRENT = "current"
    PREV = "prev"
    NEXT = "next"

    # ---------------------------------------------------------------------
    @staticmethod
    def is_list(op_name):
        return op_name == ChannelOperationType.LIST

    # ---------------------------------------------------------------------
    @staticmethod
    def is_list_sources(op_name):
        return op_name == ChannelOperationType.LIST_SOURCES

    # ---------------------------------------------------------------------
    @staticmethod
    def is_current(op_name):
        return op_name == ChannelOperationType.CURRENT

    # ---------------------------------------------------------------------
    @staticmethod
    def is_prev(op_name):
        return op_name == ChannelOperationType.PREV

    # ---------------------------------------------------------------------
    @staticmethod
    def is_next(op_name):
        return op_name == ChannelOperationType.NEXT


class ChannelType:
    """Media channel type."""

    VISUAL = "visual"
    AUDIO = "audio"

    # ---------------------------------------------------------------------
    @staticmethod
    def is_visual_channel(channel_type):
        return channel_type == ChannelType.VISUAL

    # ---------------------------------------------------------------------
    @staticmethod
    def is_audio_channel(channel_type):
        return channel_type == ChannelType.AUDIO


class Channel:
    """Represents a media channel."""

    # cache
    _content_cache = {}

    # ---------------------------------------------------------------------
    def __init__(self, channel_set, channel_name):
        """Creates a channel.

        @param channel_set Parent channel set.
        @param channel_name The name of the channel.
        """

        assert channel_name and channel_set, "Invalid arguments"

        self._channel_set = channel_set
        self._channel_name = channel_name

    # ---------------------------------------------------------------------
    def channel_type(self):
        """Returns channel type."""

        return self.channel_set().channel_type()

    # ---------------------------------------------------------------------
    def channel_name(self):
        """Returns channel name."""

        return self._channel_name

    # ---------------------------------------------------------------------
    def channel_set(self):
        """Returns the channel set that contains this channel."""

        return self._channel_set

    # ---------------------------------------------------------------------
    def set_owner(self, channel_set):
        """Sets the channels set."""

        self._channel_set = channel_set

    # ---------------------------------------------------------------------
    def directory(self, absolute=True):
        """Returns the directory of the channel."""

        return os.path.join(self.channel_set().directory(absolute), self.channel_name())

    # ---------------------------------------------------------------------
    def current_item(self):
        """Returns current item or None."""

        cur_ch = self.channel_name()
        ch_type = self.channel_type()
        cur_item = request.cookies.get("{0}-{1}-channel-item".format(cur_ch, ch_type))
        return None if cur_item is None else tornado.escape.url_unescape(cur_item)

    # ---------------------------------------------------------------------
    def content(self, first_item=None):
        """Returns the playlist."""

        return self._get_content(first_item)

    # ---------------------------------------------------------------------
    def sources(self, first_item=None):
        """@brief Returns same playlist as content returns,
        but adapted to current client needs."""

        return self._make_visual_sources([tornado.escape.url_escape(fn) for fn in self.content(first_item)])

    # ---------------------------------------------------------------------
    def _make_visual_sources(self, files):
        src = ""
        for fn in files:
            url = os.path.join(self.directory(False), fn)
            div = '<div data-src="{url}"></div>\n'.format(url=url)
            src += div

        return src

    # ---------------------------------------------------------------------
    def _is_content_cached(self):
        channel_dir = self.directory()
        return (channel_dir in Channel._content_cache and
                Channel._content_cache[channel_dir])

    # ---------------------------------------------------------------------
    def _get_content_cache(self):
        if not self._is_content_cached():
            return None

        return Channel._content_cache[self.directory()]

    # ---------------------------------------------------------------------
    def _set_content_cache(self, items):
        Channel._content_cache[self.directory()] = items

    # ---------------------------------------------------------------------
    def _get_content(self, first_item=None):
        channel_type = self.channel_set().channel_type()
        channel_name = self.channel_name()

        if not channel_name or not channel_type:
            return ""

        ch_dir = self.directory()

        if self._is_content_cached():
            files = self._get_content_cache()
            tmp = list(files)
        else:
            # fixme: make directory tree to be visible
            # content = os.walk(ch_dir)
            #
            # for dir_path, dir_names, file_names in content:
            #     pass

            tmp = sorted(os.listdir(ch_dir))
            tmp = list(filter(lambda item: item != '' and item[0] != '.' and isfile(join(ch_dir, item)), tmp))

            # logging.info("[Channel]: Channel [{0}] content [{1}]",
            #              self._channel_name, tmp)
            logging.debug(tmp)
            files = deque(tmp)
            self._set_content_cache(files)

        if first_item is not None:
            try:
                i = tmp.index(first_item)
                files.rotate(-i)
            except Exception as e:
                logging.error("[Channel]: An error [{}]; Dir [{}]".format(str(e), ch_dir))

        return files


class ChannelContent:
    """Represents a media ordered channel content.

    Used to represent channel storage on backend.
    """

    def __init__(self, content=None, directory=None):
        self._content = list(content)
        self._directory = directory
        self._i = 0

    # ---------------------------------------------------------------------
    def content(self):
        """Returns the underlying content."""

        return self._content

    # ---------------------------------------------------------------------
    def set_content(self, content):
        """Sets the underlying content."""

        self._content = content
        self._i = 0

    # ---------------------------------------------------------------------
    def next(self):
        """Moves forward."""

        if len(self._content):
            self._i += 1
            if self._i >= len(self._content):
                self._i = 0

        return self

    # ---------------------------------------------------------------------
    def prev(self):
        """Moves back."""

        if len(self._content):
            self._i -= 1
            if self._i < 0:
                self._i = len(self._content) - 1

        return self

    # ---------------------------------------------------------------------
    def current(self):
        """Returns current element."""

        if not len(self._content):
            return ""

        assert self._i < len(self._content), "Index is out of range"
        cur = self._content[self._i]
        return os.path.join(self._directory, cur) if self._directory else cur


class ChannelSet:
    """Provides channels grouped by type."""

    # cache
    _channels_list = {}

    # ---------------------------------------------------------------------
    def __init__(self, channel_type):
        """Creates a channel set.

        @param channel_type Media channel type. At present
            "visual" and "audio" are only allowed.
        """

        assert channel_type, "Invalid arguments"

        self._channel_type = channel_type

    # ---------------------------------------------------------------------
    def channel_type(self):
        """Returns channel type."""

        return self._channel_type

    # ---------------------------------------------------------------------
    def get_channel(self, channel_name):
        """Returns the channel by given name."""

        ch_list = self.channels_list()

        if channel_name in ch_list:
            return ch_list[channel_name]

        raise FileNotFoundError(
            message="Requested object not found [{0}]".format(channel_name))

    # ---------------------------------------------------------------------
    def channels_str_list(self):
        """Returns the string list containing channels."""

        # channels = self.channels_list()
        # keys = channels.keys()
        # sorted_keys = sorted(keys)
        # return sorted_keys
        return sorted(self.channels_list().keys())

    # ---------------------------------------------------------------------
    def channels_list(self):
        """Returns the channels list."""

        if not self._is_cached():
            ChannelSet._channels_list[self.channel_type()] = self._get_channels_list()
        else:
            ch_list = ChannelSet._channels_list[self.channel_type()]
            for ch in ch_list.values():
                ch.set_owner(self)

        return ChannelSet._channels_list[self.channel_type()]

    # ---------------------------------------------------------------------
    def directory(self, absolute=True):
        """Returns the channels directory.

        @param absolute Designates whether returned path is absolute or
            relative to the templates directory.
        """

        ch_dir = join(settings.CHANNELS_PATH, self.channel_type())
        return os.path.abspath(ch_dir) if absolute else os.path.normpath(ch_dir)

    # ---------------------------------------------------------------------
    def current_channel(self):
        """Returns current channel."""

        ch_list = self.channels_list()
        ch_name = self._current_channel_name()

        if ch_name[1] in ch_list:
            return ch_list[ch_name[1]]

        return None

    # ---------------------------------------------------------------------
    def current_item(self):
        """Returns current item or None."""

        ch = self.current_channel()
        if not ch:
            return None

        return ch.current_item()

    ######################################################################
    # Private stuff

    # ---------------------------------------------------------------------
    def _current_channel_name(self):
        """Returns the current channel name."""

        ch_type = self.channel_type()
        # populate channels list if necessary
        self.channels_list()

        def_ch = self._default_channel()
        param_name = ch_type + "-channel"
        cookie = request.cookies.get(param_name)

        if not cookie and def_ch:
            # req.set_cookie(param_name, def_ch)
            cookie = def_ch

        return param_name, cookie

    # ---------------------------------------------------------------------
    def _get_channels_list(self):
        d = self.directory()
        dir_list = sorted(os.listdir(d))

        # logging.info("[ChannelSet]: Channels [{0}]", str(dir_list))
        logging.debug(str(dir_list))
        channels = {ch: Channel(self, ch) for ch in dir_list if isdir(join(d, ch))}

        return channels

    # ---------------------------------------------------------------------
    def _default_channel(self):
        l = self.channels_list()
        if l and len(l) and next(iter(l)):
            return next(iter(l))
        else:
            return None

    # ---------------------------------------------------------------------
    def _is_cached(self):
        return self.channel_type() in ChannelSet._channels_list


class ChannelCollection:
    """Root object that incapsulates sets."""

    _channels = {}

    # ---------------------------------------------------------------------
    def __init__(self, root_dir):
        """Constructs a collection."""

        self._root_dir = root_dir

        d = self.directory()
        channels_list = [
            fn for fn in sorted(os.listdir(d)) if isdir(join(d, fn)) and
            (ChannelType.is_visual_channel(fn) or ChannelType.is_audio_channel(fn))
        ]

        ChannelCollection._channels = {
            ch_type: ChannelSet(ch_type) for ch_type in channels_list}

    # ---------------------------------------------------------------------
    def directory(self, absolute=True):
        root_dir = self._root_dir

        return os.path.abspath(root_dir) if absolute else os.path.normpath(root_dir)

    # ---------------------------------------------------------------------
    @staticmethod
    def channels(channel_type=None):
        if channel_type and channel_type in ChannelCollection._channels:
            return ChannelCollection._channels[channel_type]

        return ChannelCollection._channels
