#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tornado.ioloop
import tornado.web
import tornado.template
import tornado.websocket
import tornado.escape

from tornado.options import define, options
from tornado.wsgi import WSGIContainer
from tornado.web import FallbackHandler

import logging
import os
import logging.config
import os.path
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import flask_app
import settings
import req_handler
import chat_handler

logger_settings = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level':'DEBUG',
            'class':'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
}

logging.config.dictConfig(logger_settings)

class Application(tornado.web.Application):
    """Application."""

    def __init__(self):
        wsgi_container = WSGIContainer(flask_app.app)

        tornado_handlers = [
            (r'/chat/$', chat_handler.WebSocket),
            (r".*$", FallbackHandler, dict(fallback=wsgi_container)),
        ]
        app_dir = os.path.abspath(os.path.dirname(__file__))
        opts = dict(
            template_path=os.path.join(app_dir, settings.TEMPLATE_PATH),
            static_path=os.path.join(app_dir, settings.STATIC_PATH),
            xsrf_cookies=False,
            cookie_secret="11oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            debug=True,
            autoreload=False,
        )
        tornado.web.Application.__init__(self, tornado_handlers, **opts)
        flask_app.chat_clients = self.chat_clients = set()

# main entry point
def main():
    define("address", default="127.0.0.1", help="Run on the given ip")
    define("port", default=8889, help="Run on the given port", type=int)

    options.parse_command_line()

    # tornado server app
    app = Application()
    app.listen(options.port, options.address)

    print("Starting...")
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print("An error occurred: {0}".format(str(e)))
    except:
        print("An unknown error occurred!")


