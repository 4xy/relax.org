import os.path

EN_LOCALE = "en"
RU_LOCALE = "ru"
BG_LOCALE = "bg"
DEFAULT_LOCALE = BG_LOCALE
LOCALES = [EN_LOCALE, BG_LOCALE, RU_LOCALE]

TEMPLATE_PATH = 'templates'
STATIC_PATH = 'static'
CHANNELS_DIR_NAME = 'channels'
CHANNELS_PATH = os.path.join(STATIC_PATH, CHANNELS_DIR_NAME)

SITE_DOMAIN = "relax4u.org"
NO_REPLY_EMAIL = "noreply@" + SITE_DOMAIN
CONTACT_EMAIL = "noreply@relax4u.org"
